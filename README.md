# SquareSpace accessibility remediation

A toolkit for folks trying to make SquareSpace sites acheive WCAG 2 AA compliance.

Please do send me your code or a pull request to update or expand this kit. And if you know of accessible out-of-the box templates, do send them over. I'd love a list.



## General Advice

* Read SquareSpace's own [accessibility guide](https://support.squarespace.com/hc/en-us/articles/215129127-Making-your-Squarespace-site-more-accessible) for their latest how-tos. But note that it's focused on best practices for *content*, not *the template.* Doing everything they recommend won't finish the job.
* [Add alt text to images](https://support.squarespace.com/hc/en-us/articles/206542357). But note that some templates don't support alt text on certain images, and will always display the filename instead. This seems especially common for big panel banner images. You can sometimes get a serviceable alt with a filename hack (e.g., "This is my alternative text.jpg"). If you do that, add the "Filename as alt" hack below in your JavaScript to remove the filename. When all else fails, you can use a JS hack to hide a decorative image (below).
* [Semantically structure your headings](https://dequeuniversity.com/assets/html/jquery-summit/html5/slides/headings.html).
* Pick [accessible colors](https://webaim.org/resources/contrastchecker/)



## Hacks

### Hover & Focus indicators
Many templates lack hover and focus states. Write your own or steal this: 
```html
    <style>
      a:hover, a:focus, button:focus, button:hover {text-decoration: underline !important; }
    </style>
```
### Social media icons missing labels
Many templates have a set of social media icons in a block, with no alternative text. This will iterate through them and add an aria-label. Duplicate one of the else if statements if your site has a link not on this list.
```html
    <script>
        document.addEventListener("DOMContentLoaded", function(){
            var labelSocial = document.querySelectorAll('a.sqs-svg-icon--wrapper');
            if (labelSocial.length) {
                for (var i = 0; i < labelSocial.length; i++) {
                  var myLabel = "";
                  if (labelSocial[i].classList.contains("twitter")) {
                    myLabel = 'Twitter';
                  } else if (labelSocial[i].classList.contains("email")) {
                    myLabel = 'Email';
                  } else if (labelSocial[i].classList.contains("facebook")) {
                    myLabel = 'Facebook';
                  } else if (labelSocial[i].classList.contains("instagram")) {
                    myLabel = 'Instagram';
                  }
                  labelSocial[i].setAttribute('aria-label', myLabel );
                }
            }
        }
    </script>
```
### Decorative elements which should be hidden from screen readers
Change the selector to whatever you want to hide. This can be a list of unrelated things -- e.g., ".class1, .class2, #id1, #id2 .class3 img"
```html
    <script>
        document.addEventListener("DOMContentLoaded", function(){
            var hideMe = document.querySelectorAll('.class-name-to-hide');
            if (hideMe.length) {
                for (var i = 0; i < hideMe.length; i++) {
                  hideMe[i].setAttribute('aria-hidden','true');
                }
            }
        }
    </script>
```
### Menu hover flyouts don't work on focus
Many template menus don't respond to keyboard navigation, only mouse hovers. 

This particular example maps to the Hayden template's classes. Note that you'll have to modify this code with the classes of your menu. 
```html
    <style>
        .Header-nav .Header-nav-item--folder a:focus~.Header-nav-folder, 
        .focused .Header-nav-folder {
            left:0 !important; 
            opacity:1 !important;
        }
    </style>
    <script>
        document.addEventListener("DOMContentLoaded", function(){
            var menuToggles = document.querySelectorAll('.Header-nav a');
            if (menuToggles.length) {
                for (var i = 0; i < menuToggles.length; i++) {
                    menuToggles[i].onfocus = menuFocus;
                }
            }
            function menuFocus() {
                var focused = document.querySelector('.focused');
                if (focused) {
                  focused.classList.remove('focused');
                }
                document.activeElement.closest('.Header-nav-item--folder').classList.add('focused');
              }
        }
    </script>
```
### Fake toggle can't be operated with assistive devices
Many templates are using images with onClick events as mobile menu toggles. These only respond to mouse clicks, not keyboard keypresses. This function grabs a particular fake button (your class will need to match), gives it an aria label, makes it focusable, and makes it clickable from a keyboard. 

```html
    <script>
        document.addEventListener("DOMContentLoaded", function(){
            var yoMenu = document.querySelectorAll('.mobile-nav-toggle');
                if (yoMenu.length) {
                    for (var i = 0; i < yoMenu.length; i++) {
                        yoMenu[i].setAttribute('aria-label','Toggle menu');
                        yoMenu[i].setAttribute('aria-role','button');
                        yoMenu[i].setAttribute('tabindex','0');
                        yoMenu[i].setAttribute('onKeyPress','handleBtnKeyPress(event)');
        
                    }
                }
            });  
        function handleBtnKeyPress(event) {
              if (event.key === " " || event.key === "Enter") {
                // Prevent the default action to stop scrolling when space is pressed
                event.preventDefault();
                document.activeElement.click();
              }
            }
        }
    </script>
```


### Filename-as-alt hack leaves .jpg in alt text
Uploading an image named "A prancing unicorn.jpg" gives you a decent alt, but the screen reader will announce "dot jay pee gee" at the end. Let's fix that.

```html
    <script>
        document.addEventListener("DOMContentLoaded", function(){
            var removeFileType = document.querySelectorAll('img[alt$=\".jpg\"');
                if (removeFileType.length) {
                    for (var i = 0; i < removeFileType.length; i++) {
                          var dirtyAlt = removeFileType[i].getAttribute('alt');
                          dirtyAlt = dirtyAlt.slice(0,-4);
                          removeFileType[i].setAttribute('alt', dirtyAlt);
                    }
                }
        });
    </script>
```


## Tested templates
### Compliant out of the box
* ?

### Known to be fixable
* Hayden
* Five

### Had blockers when last tested
* *Greenwich's* menu fly-out code makes navigation with assistive devices extremely confusing. Its subfolders are not nested within their parent, and the mobile slide-right-left navigation paradigm does not reassign focus -- meaning if a user clicks on an item to unfold its children and then presses Tab to advance to the child...they instead advance to the next parent, and have to tab through the entire list of parents before hitting the lists of children. It may be possible to remediate it this in the JS layer, but I generally recommend not using this template if you need to have menu folders.
* [Bedford](https://bedford-demo.squarespace.com) has fake menu parent items (they are divs, not links or buttons) that act as flyouts on hover on desktop and clicktoggles on mobile. You'll need to combine the Menu hover flyout hack with the fake toggle hack. Drop the aria-label on the latter, as the menu item text will cover.